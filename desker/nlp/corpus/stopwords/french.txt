être
avoir
a
au
aux
avec
ce
ces
dans
de
des
du
elle
en
et
eux
il
je
la
le
leur
lui
ma
mais
me
même
mes
moi
mon
ne
nos
notre
nous
on
ou
où
par
pas
pour
qu
que
qui
sa
se
ses
son
sur
ta
te
tes
toi
ton
tu
un
une
vos
votre
vous
c
d
j
l
à
m
n
s
t
y
été
étée
étées
étés
étant
suis
es
est
sommes
êtes
sont
serai
seras
sera
serons
serez
seront
serais
serait
serions
seriez
seraient
étais
était
étions
étiez
étaient
fus
fut
fûmes
fûtes
furent
sois
soit
soyons
soyez
soient
fusse
fusses
fût
fussions
fussiez
fussent
ayant
eu
eue
eues
eus
ai
as
avons
avez
ont
aurai
auras
aura
aurons
aurez
auront
aurais
aurait
aurions
auriez
auraient
avais
avait
avions
aviez
avaient
eut
eûmes
eûtes
eurent
aie
aies
ait
ayons
ayez
aient
eusse
eusses
eût
eussions
eussiez
eussent
ceci
cela
cet
cette
ici
ils
les
leurs
quel
quels
quelle
quelles
sans
soi
autant
voire
au
aux
avec
ce
ces
dans
de
des
du
elle
en
et
eux
il
je
la
le
leur
lui
ma
mais
me
même
mes
moi
mon
ne
nos
notre
nous
on
ou
par
pas
pour
qu
que
qui
sa
se
ses
son
sur
ta
te
tes
toi
ton
tu
un
une
vos
votre
vous
c
d
j
l
à
m
n
s
t
y
été
étée
étées
étés
étant
suis
es
est
sommes
êtes
sont
serai
seras
sera
serons
serez
seront
serais
serait
serions
seriez
seraient
étais
était
étions
étiez
étaient
fus
fut
fûmes
fûtes
furent
sois
soit
soyons
soyez
soient
fusse
fusses
fût
fussions
fussiez
fussent
ayant
eu
eue
eues
eus
ai
as
avons
avez
ont
aurai
auras
aura
aurons
aurez
auront
aurais
aurait
aurions
auriez
auraient
avais
avait
avions
aviez
avaient
eut
eûmes
eûtes
eurent
aie
aies
ait
ayons
ayez
aient
eusse
eusses
eût
eussions
eussiez
eussent
ceci
celà
cet
cette
ici
ils
les
leurs
quel
quels
quelle
quelles
sans
soi
a
à
â
abord
afin
ah
ai
aie
ainsi
allaient
allo
allô
allons
après
assez
attendu
au
aucun
aucune
aujourd
aujourd'hui
auquel
aura
auront
aussi
autre
autres
aux
auxquelles
auxquels
avaient
avais
avait
avant
avec
avoir
ayant
b
bah
beaucoup
bien
bigre
boum
bravo
brrr
c
ça
car
ce
ceci
cela
celle
celle-ci
celle-là
celles
celles-ci
celles-là
celui
celui-ci
celui-là
cent
cependant
certain
certaine
certaines
certains
certes
ces
cet
cette
ceux
ceux-ci
ceux-là
chacun
chaque
cher
chère
chères
chers
chez
chiche
chut
ci
cinq
cinquantaine
cinquante
cinquantième
cinquième
clac
clic
combien
comme
comment
compris
concernant
contre
couic
crac
d
da
dans
de
debout
dedans
dehors
delà
depuis
derrière
des
dès
désormais
desquelles
desquels
dessous
dessus
deux
deuxième
deuxièmement
devant
devers
devra
différent
différente
différentes
différents
dire
divers
diverse
diverses
dix
dix-huit
dixième
dix-neuf
dix-sept
doit
doivent
donc
dont
douze
douzième
dring
du
duquel
durant
e
effet
eh
elle
elle-même
elles
elles-mêmes
en
encore
entre
envers
environ
es
ès
est
et
etant
étaient
étais
était
étant
etc
été
etre
être
eu
euh
eux
eux-mêmes
excepté
f
façon
fais
faisaient
faisant
fait
feront
fi
flac
floc
font
g
gens
h
ha
hé
hein
hélas
hem
hep
hi
ho
holà
hop
hormis
hors
hou
houp
hue
hui
huit
huitième
hum
hurrah
i
il
ils
importe
j
je
jusqu
jusque
k
l
la
là
laquelle
las
le
lequel
les
lès
lesquelles
lesquels
leur
leurs
longtemps
lorsque
lui
lui-même
m
ma
maint
mais
malgré
me
même
mêmes
merci
mes
mien
mienne
miennes
miens
mille
mince
moi
moi-même
moins
mon
moyennant
n
na
ne
néanmoins
neuf
neuvième
ni
nombreuses
nombreux
non
nos
notre
nôtre
nôtres
nous
nous-mêmes
nul
o
o|
ô
oh
ohé
olé
ollé
on
ont
onze
onzième
ore
ou
où
ouf
ouias
oust
ouste
outre
p
paf
pan
par
parmi
partant
particulier
particulière
particulièrement
pas
passé
pendant
personne
peu
peut
peuvent
peux
pff
pfft
pfut
pif
plein
plouf
plus
plusieurs
plutôt
pouah
pour
pourquoi
premier
première
premièrement
près
proche
psitt
puisque
q
qu
quand
quant
quanta
quant-à-soi
quarante
quatorze
quatre
quatre-vingt
quatrième
quatrièmement
que
quel
quelconque
quelle
quelles
quelque
quelques
quelqu'un
quels
qui
quiconque
quinze
quoi
quoique
r
revoici
revoilà
rien
s
sa
sacrebleu
sans
sapristi
sauf
se
seize
selon
sept
septième
sera
seront
ses
si
sien
sienne
siennes
siens
sinon
six
sixième
soi
soi-même
soit
soixante
son
sont
sous
stop
suis
suivant
sur
surtout
t
ta
tac
tant
te
té
tel
telle
tellement
telles
tels
tenant
tes
tic
tien
tienne
tiennes
tiens
toc
toi
toi-même
ton
touchant
toujours
tous
tout
toute
toutes
treize
trente
très
trois
troisième
troisièmement
trop
tsoin
tsouin
tu
u
un
une
unes
uns
v
va
vais
vas
vé
vers
via
vif
vifs
vingt
vivat
vive
vives
vlan
voici
voilà
vont
vos
votre
vôtre
vôtres
vous
vous-mêmes
vu
w
x
y
z
zut
au
aux
avec
ce
ces
dans
de
des
du
elle
en
et
eux
il
je
la
le
leur
lui
ma
mais
me
même
mes
moi
mon
ne
nos
notre
nous
on
ou
par
pas
pour
qu
que
qui
sa
se
ses
son
sur
ta
te
tes
toi
ton
tu
un
une
vos
votre
vous
c
d
j
l
à
m
n
s
t
y
été
étée
étées
étés
étant
suis
es
est
sommes
êtes
sont
serai
seras
sera
serons
serez
seront
serais
serait
serions
seriez
seraient
étais
était
étions
étiez
étaient
fus
fut
fûmes
fûtes
furent
sois
soit
soyons
soyez
soient
fusse
fusses
fût
fussions
fussiez
fussent
ayant
eu
eue
eues
eus
ai
as
avons
avez
ont
aurai
auras
aura
aurons
aurez
auront
aurais
aurait
aurions
auriez
auraient
avais
avait
avions
aviez
avaient
eut
eûmes
eûtes
eurent
aie
aies
ait
ayons
ayez
aient
eusse
eusses
eût
eussions
eussiez
eussent
ceci
cela
celà
cet
cette
ici
ils
les
leurs
quel
quels
quelle
quelles
sans
soi
alors
au
aucuns
aussi
autre
avant
avec
avoir
bon
car
ce
cela
ces
ceux
chaque
ci
comme
comment
dans
des
du
dedans
dehors
depuis
deux
devrait
doit
donc
dos
droite
début
elle
elles
en
encore
essai
est
et
eu
fait
faites
fois
font
force
haut
hors
ici
il
ils
je
juste
la
le
les
leur
là
ma
maintenant
mais
mes
mine
moins
mon
mot
même
ni
nommés
notre
nous
nouveaux
ou
où
par
parce
parole
pas
personnes
peut
peu
pièce
plupart
pour
pourquoi
quand
que
quel
quelle
quelles
quels
qui
sa
sans
ses
seulement
si
sien
son
sont
sous
soyez
sujet
sur
ta
tandis
tellement
tels
tes
ton
tous
tout
trop
très
tu
valeur
voie
voient
vont
votre
vous
vu
ça
étaient
état
étions
été
être
au
aux
avec
ce
ces
dans
de
des
du
elle
en
et
eux
il
je
la
le
leur
lui
ma
mais
me
même
mes
moi
mon
ne
nos
notre
nous
on
ou
par
pas
pour
qu
que
qui
sa
se
ses
son
sur
ta
te
tes
toi
ton
tu
un
une
vos
votre
vous
c
d
j
l
à
m
n
s
t
y
été
étée
étées
étés
étant
suis
es
est
sommes
êtes
sont
serai
seras
sera
serons
serez
seront
serais
serait
serions
seriez
seraient
étais
était
étions
étiez
étaient
fus
fut
fûmes
fûtes
furent
sois
soit
soyons
soyez
soient
fusse
fusses
fût
fussions
fussiez
fussent
ayant
eu
eue
eues
eus
ai
as
avons
avez
ont
aurai
auras
aura
aurons
aurez
auront
aurais
aurait
aurions
auriez
auraient
avais
avait
avions
aviez
avaient
eut
eûmes
eûtes
eurent
aie
aies
ait
ayons
ayez
aient
eusse
eusses
eût
eussions
eussiez
eussent
ceci
celà
cet
cette
ici
ils
les
leurs
quel
quels
quelle
quelles
sans
soi
étés
sont
auront
ayez
serait
quand
être
t
comme
as
tandis
nous
mien
aurait
serons
peu
dehors
notre
étant
ayant
avaient
ça
on
se
ou
l
pourquoi
auriez
donc
aurez
vous
sois
même
vos
aurai
m
suis
eusses
eux
moins
quelles
ayants
ses
me
peut
alors
une
seriez
en
essai
n
la
les
pour
ayante
eût
eusse
étaient
soient
et
eussent
de
fussiez
aux
depuis
fois
toi
fait
autre
aura
font
car
son
serez
aient
doit
mon
état
étantes
ni
eûmes
aurais
eussions
avais
tels
elle
dans
sien
ma
ayantes
cette
avez
tous
ait
étante
d'une
ce
tes
ci
eurent
devrait
seront
étants
plusieurs
du
trop
sur
faites
si
vont
y
nos
ces
s
fussent
eûtes
avant
il
je
fûtes
mais
soyez	sujet
j
comment
serai
avec
quelle
te
ne
d
fusse
sommes
quels
je	juste
là
aurions
mes
fûmes
sera
moi
fus
avions
ayons
eue
encore
serions
ta
eu
sa
le
nommés
étiez
ils
quel
où
avoir
leur
eues
cela
ton
avons
eut
qu
fût
eus
par
seraient
es
ont
soyons
lui
pas
serais
aie
parce
tellement
étée
maintenant
plupart
très
seulement
fut
ai
dos
aviez
été
vu
a
plus
fusses
furent
ici
elles
hors
soit
voient
avait
étais
votre
tu
ceux
au
est
qui
début
sous
sans
des
un
fussions
aurons
tout
c
eussiez
soyez
à
étions
seras
bon
étées
aies
auras
aucuns
était
dedans
êtes
aussi
auraient
chaque
que
mot
Un
c'est
modifier
afin
wikipedia
wikimedia
accueil
contact
article
discussion
contributeurs
créer
creer
compte
connecter
se
don
outils
pages
liées
suivi
voir
lien
permanent
informations
quelques
version
imprimable
modifier
assez
généralement
seras
bon
étées
aies
auras
aucuns
était
dedans
êtes
