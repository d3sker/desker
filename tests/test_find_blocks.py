import os

import pytest
from PIL import Image

from desker.ocr.detect import find_blocks


def test_find_blocks():
    # example to find paragraphs
    print("Finding paragraphs".ljust(40), end='')
    html = os.path.join(pytest.RESOURCES_DIR,
                        "links/2_screenshot.png")
    img = Image.open(html).convert("RGB")
    sub, masks = find_blocks(img, max_aspect=15.,
                             max_occupation=0.2,
                             min_occupation=0.05,
                             min_aspect=1 / 15)
    # for s in sub:
    #     s.show()
    print(pytest.OK)

    # example to find a window
    print("Finding window".ljust(40), end='')
    screen = os.path.join(pytest.RESOURCES_DIR,
                          "icon_images/test_img/0009_screenshot.png")
    img = Image.open(screen).convert("RGB")
    sub, masks = find_blocks(img, max_aspect=3.,
                             max_occupation=0.5,
                             min_occupation=0.05,
                             min_aspect=1 / 3)
    # for s in sub:
    #     s.show()
    print(pytest.OK)
