import os

import numpy as np
import pandas as pd
import pytest
from PIL import Image

from desker.login.knn import (get_distances, import_model,
                              predict_proba_from_image)
from desker.login.knn.predict import __max_dist_threshold__
from desker.login.knn.train import learn, save

model_file = "/tmp/model.knn"


def test_learn_and_save() -> None:
    d = os.path.join(pytest.RESOURCES_DIR, "login/")
    csv = os.path.join(d, "labels.csv")
    # learn the model
    knn = learn(csv, d)
    # save it to a file
    save(knn, model_file)


def test_model() -> None:
    d = os.path.join(pytest.RESOURCES_DIR, "login/")
    # load model within login library
    import_model(model_file)
    P = pd.read_csv(os.path.join(d, "labels.csv"))

    for _, record in P.iterrows():
        print(record.img, end='\t')
        img = os.path.join(d, record.img)
        target = record.label
        res = predict_proba_from_image(Image.open(img))
        label = max(res.keys(), key=lambda i: res[i])
        if label == target:
            print(pytest.OK)
        else:
            print(pytest.ERROR)


def test_on_desktop_images() -> None:

    d = os.path.join(pytest.RESOURCES_DIR, "nologin/")
    for img_file in filter(lambda f: f.endswith('.png'), os.listdir(d)):
        print(img_file, end='\t')
        img = os.path.join(d, img_file)
        min_dist = np.min(get_distances(Image.open(img)))
        print("{:.3f}".format(min_dist), end='\t')
        if img_file == "0007_screenshot.png":
            assert min_dist <= __max_dist_threshold__
            print(pytest.OK)
        else:
            assert min_dist > __max_dist_threshold__
            print(pytest.OK)
