from desker.nlp.algorithm import Rake

text = "La démarchandisation (ou décommodification) est le processus par lequel la sphère du marché se réduit pour laisser place à un engagement de la force publique. La démarchandisation vise à réduire la dépendance des individus face au marché et à assurer les risques des citoyens, c'est-à-dire à lui assurer une autonomie vis-à-vis du marché1. C'est le mouvement inverse de la marchandisation, qui vise à faire de tout bien un bien privé, vendable."


def test_rake_001():
    rake = Rake()
    keywords = rake.fit(text)
    assert "La démarchandisation" in keywords
    assert "décommodification" in keywords


def test_rake_002():
    rake = Rake(max_words=2, min_characters=10)
    keywords = rake.fit(text)
    for keyword in keywords:
        assert len(keyword.split()) <= 2
        assert len(keyword) >= 10
