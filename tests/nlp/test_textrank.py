from desker.nlp.algorithm import TextRank

text = """L'histoire d'Alexandrie commence avec sa fondation par Alexandre le Grand en 331 av. J.-C.. Auparavant, se trouvaient déjà de grandes villes portuaires à l'est du futur emplacement d'Alexandrie, à l'ouest de la présente baie d'Aboukir. La branche du delta du Nil la plus à l'ouest existait encore à l'époque et était utilisée pour le voyagement de marchandises.
Après sa fondation, la ville devient le chef-lieu de la dynastie des Ptolémées d'Égypte et grandit rapidement pour devenir l'une des villes les plus importantes de l'époque hellénistique, dépassée seulement par Rome en grandeur et richesse.
Elle passe aux Arabes en 641 et une nouvelle capitale égyptienne, Fostat, est fondée sur le Nil. Après sa perte de statut de capitale, Alexandrie tombe dans un long déclin jusque vers la fin du règne de l'Empire ottoman alors qu'elle n'est guère plus qu'un petit village de pêcheurs. Au début du xixe siècle, la ville est ravivée par Méhémet Ali, le vice-roi d'Égypte, dans le cadre d'un programme d'industrialisation.
La ville actuelle est le port principal du pays ainsi qu'un centre commercial, de tourisme et de transport. C'est une métropole industrielle, centre d'une région où sont raffinés ou produits pétrole, asphalte, textiles à base de coton, nourriture industrialisée, papier, matière plastiques et polystyrène."""


def test_textrank_001():
    text_rank = TextRank()
    keywords_score = text_rank.fit(text)
    print("text rank")
    print(keywords_score)
