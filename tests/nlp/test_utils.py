from desker.nlp import utils


def test_tokenize_001():
    text = "One two three Four fivesix 1984 ! :)"
    tokens = utils.tokenize(text)

    should_be = ["One", "two", "three", "Four", "fivesix", "1984"]
    assert tokens == should_be


def test_tokenize_002():
    text = "One tWo three F04r fivEsix ! :) 1984"
    tokens = utils.tokenize(text, lower=True)

    should_be = ["one", "two", "three", "f04r", "fivesix", "1984"]
    assert tokens == should_be


def test_stemming():
    stems = utils.stem_sentence("les voitures seraient rouges")

    should_be = "le voitur ser roug"
    assert stems == should_be


def test_split_text_by_delimiters():
    text = """The Main Sawmill, now known as Ledyard Up-Down Sawmill !!!!!!!! is a historic 19th-century sawmill at 175 Iron Street in Ledyard, Connecticut. The sawmill was built in 1869 by Israel Brown, and is the only known operational mill of this type in the state. It was listed on the National Register of Historic Places in 1972. It is now ? owned by the town - and administered by the local historical society as a museum."""
    sentences = utils.split_text_by_delimiters(text)

    should_be = [
        "The Main Sawmill",
        " now known as Ledyard Up-Down Sawmill ",
        " is a historic 19th-century sawmill at 175 Iron Street in Ledyard",
        " Connecticut",
        " The sawmill was built in 1869 by Israel Brown",
        " and is the only known operational mill of this type in the state",
        " It was listed on the National Register of Historic Places in 1972",
        " It is now ",
        " owned by the town",
        "and administered by the local historical society as a museum",
    ]
    assert sentences == should_be


def test_split_text_by_stopwords_001():
    text = "Jean-Camille Bellaigue (2 avril 1893, Paris - 10 octobre 1931, Paris), est     un dessinateur publicitaire, illustrateur de livres, aquarelliste, graveur et médailleur        français."
    sentences = utils.split_text_by_stopwords(text)

    should_be = [
        "Jean Camille Bellaigue 2 avril 1893 Paris 10 octobre 1931 Paris",
        "dessinateur publicitaire illustrateur",
        "livres aquarelliste graveur",
        "médailleur français",
    ]
    assert sentences == should_be


def test_remove_stopwords():
    text = "La démarchandisation vise à réduire la dépendance des individus face au marché et à assurer les risques des citoyens, c'est-à-dire à lui assurer une autonomie vis-à-vis du marché."
    tokens = utils.tokenize(text)
    tokens = utils.remove_stopwords(tokens)

    should_be = [
        "démarchandisation",
        "vise",
        "réduire",
        "dépendance",
        "individus",
        "face",
        "marché",
        "assurer",
        "risques",
        "citoyens",
        "assurer",
        "autonomie",
        "vis",
        "vis",
        "marché",
    ]
    assert tokens == should_be
