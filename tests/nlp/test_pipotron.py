from desker.nlp.pipotron.bow import BagOfWords
from desker.nlp.pipotron.template import Template


def test_pipotron():
    laboralphy = Template("laboralphy.yml")
    laboralphy.render()
    mail = Template("email.yml")
    mail_to = "John Doe"
    mail_from = "Donald T."
    BagOfWords().add("destinataire", mail_to)
    BagOfWords().add("emetteur", mail_from)
    mail_content = mail.render()

    assert mail_to in mail_content
    assert mail_from in mail_content
