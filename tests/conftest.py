import os
from typing import Tuple

import pytest

from desker import faster_rcnn
from desker.app import app
from desker.login import cnn, knn

pytest.TEST_DIR = os.path.dirname(__file__)
pytest.RESOURCES_DIR = os.path.join(pytest.TEST_DIR, "resources/")
pytest.OK = "[\033[32;1mOK\033[0m]"
pytest.WARNING = "[\033[33;1mWARNING\033[0m]"


def pytest_runtest_logstart(nodeid: str, location: Tuple[str, int, str]):
    """
    Signal the start of running a single test item.

    This hook will be called **before** :func:`pytest_runtest_setup`,
    :func:`pytest_runtest_call` and
    :func:`pytest_runtest_teardown` hooks.

    Parameters
    ----------
    nodeid : str
        full id of the item
    location : Tuple[str, int, str]
        a triple of ``(filename, linenum, testname)``
    """
    print("")


@pytest.fixture(scope='module')
def client():
    knn.import_model()
    cnn.load_model()
    faster_rcnn.load_model()
    yield app.test_client()


@pytest.fixture(scope="function")
def library_banner():
    print("""\033[96m
██╗     ██╗██████╗ ██████╗  █████╗ ██████╗ ██╗   ██╗
██║     ██║██╔══██╗██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
██║     ██║██████╔╝██████╔╝███████║██████╔╝ ╚████╔╝
██║     ██║██╔══██╗██╔══██╗██╔══██║██╔══██╗  ╚██╔╝
███████╗██║██████╔╝██║  ██║██║  ██║██║  ██║   ██║
╚══════╝╚═╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝
\033[0m""")
