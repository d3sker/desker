import io
import os
from http import HTTPStatus

import pytest
from PIL import Image
from PIL.PngImagePlugin import PngImageFile

login = os.path.join(pytest.RESOURCES_DIR,
                     "login/0000_screenshot.png")
screenshot = os.path.join(pytest.RESOURCES_DIR,
                          "icon_images/test_img/0009_screenshot.png")
link = os.path.join(pytest.RESOURCES_DIR,
                    "links/2_screenshot.png")


def test_library(library_banner):
    # fancy banner display
    pass


def test_os_detection(client):
    r = client.post("/os_detection/from_login",
                    data={"file": open(screenshot, "rb")})
    assert r.status_code == HTTPStatus.OK
    r = client.post("/os_detection/from_login",
                    data={"file": open(screenshot, "rb"),
                          "proba": True})
    assert r.status_code == HTTPStatus.OK

    # -------------------------------------------------- #
    r = client.post("/os_detection/is_login",
                    data={"file": open(login, "rb")})
    assert r.status_code == HTTPStatus.OK
    assert r.json["is_login"]
    r = client.post("/os_detection/is_login",
                    data={"file": open(login, "rb"),
                          "proba": True})
    assert r.status_code == HTTPStatus.OK
    assert r.json["is_login"]


def test_object_detection(client):
    r = client.post("/object_detection/all",
                    data={"file": open(screenshot, "rb")})
    assert r.status_code == HTTPStatus.OK
    r = client.post("/object_detection/all",
                    data={"file": open(screenshot, "rb"),
                          "filter": True})
    assert r.status_code == HTTPStatus.OK

    threshold = 0.25
    r = client.post("/object_detection/all",
                    data={"file": open(screenshot, "rb"),
                          "filter": True,
                          "threshold": threshold})
    assert r.status_code == HTTPStatus.OK
    for s in r.json["scores"]:
        assert s >= threshold

    # -------------------------------------------------- #
    r = client.post("/object_detection/all/demo",
                    data={"file": open(screenshot, "rb"),
                          "threshold": threshold})
    assert r.status_code == HTTPStatus.OK

    buffer = io.BytesIO(r.get_data())
    img = Image.open(buffer)
    assert isinstance(img, PngImageFile)


def test_text_detection(client):
    r = client.post("/text_detection/all",
                    data={"file": open(link, "rb")})
    assert r.status_code == HTTPStatus.OK

    r = client.post("/text_detection/links",
                    data={"file": open(link, "rb")})
    assert r.status_code == HTTPStatus.OK

    r = client.post("/text_detection/links",
                    data={"file": open(link, "rb"),
                          "link_colors": "#0645AD,#0645AD"})
    assert r.status_code == HTTPStatus.OK

    r = client.post("/text_detection/blocks",
                    data={"file": open(link, "rb")})
    assert r.status_code == HTTPStatus.OK
