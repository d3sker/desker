import os

import pytest
import torch
from PIL import Image
from torchvision.models.detection.faster_rcnn import FasterRCNN

from desker.faster_rcnn.predict import load_model, predict_elements
from desker.faster_rcnn.train import get_FasterRCNN_model, train

# tmp files
__test_model_path = "/tmp/model.frcnn"
__test_classes_path = "/tmp/labels.json"


def test_get_FasterRCNN_model():
    assert isinstance(get_FasterRCNN_model(1), FasterRCNN)
    print('get_FasterRCNN_model_test.OK')


def test_train():
    annotations_test = os.path.join(
        pytest.RESOURCES_DIR,
        'icon_images/annotations_example.json')
    epochs_test = 1
    batch_size_test = 1
    device_test = 'cpu'

    print("Training Faster RCNN model...", end='\t')
    model, dataset = train(
        annotations_test,
        epochs_test,
        batch_size_test,
        device_test)
    print(pytest.OK)

    print(f"Saving model to {__test_model_path} ... ", end='\t')
    torch.save(model.state_dict(), __test_model_path)
    print(pytest.OK)

    print(f"Saving labels to {__test_classes_path} ... ", end='\t')
    dataset.save_labels(__test_classes_path)
    print(pytest.OK)


def test_pred_and_load():
    print("")
    load_model(__test_model_path, __test_classes_path)
    img_file = os.path.join(
        pytest.RESOURCES_DIR,
        'icon_images/test_img/0056_screenshot.png')
    img_test = Image.open(img_file)
    print(f"Prediction on {img_file} ...", end='\t')
    predict_elements(img_test)
    print(pytest.OK)
