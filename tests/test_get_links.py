import os

import pytest
from PIL import Image, ImageDraw

from desker.ocr.detect import get_links


def test_get_links() -> None:

    d = os.path.join(pytest.RESOURCES_DIR, "links/")
    for f in os.listdir(d):
        img = Image.open(os.path.join(d, f))
        draw = ImageDraw.Draw(img)

        res = get_links(img)
        for _, x, y in res:
            draw.line([(x - 5, y - 5), (x + 5, y + 5)], fill=128)
            draw.line([(x - 5, y + 5), (x + 5, y - 5)], fill=128)

        res = get_links(img, result="boxes")
        for top, left, height, width, _ in res:
            draw.rectangle([(left, top), (left + width, top + height)],
                           outline=128)
        img.save("/tmp/{:s}".format(f))


def test_get_links_auto() -> None:

    d = os.path.join(pytest.RESOURCES_DIR, "links/")
    for f in os.listdir(d):
        print(f)
        img = Image.open(os.path.join(d, f)).convert("RGB")
        draw = ImageDraw.Draw(img)

        # res = get_links(img, link_colors="auto", result="centers")
        # for _, x, y in res:
        #     draw.line([(x - 5, y - 5), (x + 5, y + 5)], fill=128)
        #     draw.line([(x - 5, y + 5), (x + 5, y - 5)], fill=128)

        res = get_links(img, link_colors="auto", result="boxes")
        for top, left, height, width, _ in res:
            x = left + 0.5 * width
            y = top + 0.5 * height
            draw.rectangle([(left, top), (left + width, top + height)],
                           outline=128)
            draw.line([(x - 5, y - 5), (x + 5, y + 5)], fill=128)
            draw.line([(x - 5, y + 5), (x + 5, y - 5)], fill=128)

        img.save("/tmp/{:s}".format(f))
