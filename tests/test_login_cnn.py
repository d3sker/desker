
import inspect
import os

import pytest
from PIL import Image, UnidentifiedImageError

from desker.login.cnn.predict import (is_login_page, load_model,
                                      login_page_probability)
from desker.login.cnn.train import main

login_dir = os.path.join(pytest.RESOURCES_DIR, "login")
nologin_dir = os.path.join(pytest.RESOURCES_DIR, "nologin")


def test_train():
    raw_args = f'-e 1 -d cpu {login_dir} {nologin_dir}'.split(" ")
    main(raw_args)


def test_predict():
    print()
    base_dir = os.path.dirname(inspect.getfile(is_login_page))
    load_model(os.path.join(base_dir, "model.cnn"))

    login_score = 0
    total = 0
    for f in os.listdir(login_dir):
        path = os.path.join(login_dir, f)
        try:
            print(f, end='\t')
            img = Image.open(path).convert("RGB")
            status = is_login_page(img)
            p = login_page_probability(img)
            print(
                f"({100. * p:.2f}%)\t",
                pytest.OK if status else pytest.WARNING)
            login_score += status
            total += 1
        except UnidentifiedImageError:
            print(f"Ignoring file {f}")
            continue
    print(f"Login screenshot score: {login_score}/{total}")

    nologin_score = 0
    total = 0
    for f in os.listdir(nologin_dir):
        path = os.path.join(nologin_dir, f)
        try:
            print(f, end='\t')
            img = Image.open(path).convert("RGB")
            status = is_login_page(img)
            p = login_page_probability(img)
            print(
                f"({100. *(1.- p):.2f}%)\t",
                pytest.WARNING if status else pytest.OK)
            nologin_score += status
            total += 1
        except UnidentifiedImageError:
            print(f"Ignoring file {f}")
            continue
    print(f"No-Login screenshot score: {total-nologin_score}/{total}")
