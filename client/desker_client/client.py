#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 6 11:00:00 2020

@author: asr
"""

import io
import json
import time
from http import HTTPStatus
from typing import Callable, Dict, List, Tuple, Union

import numpy as np
import requests
from PIL import Image
from PIL.Image import Image as ImageType


def enforce_bytes(func: Callable) -> Callable:
    """
    Decorator to turn image (file path or PIL image)
    into a raw bytes buffer
    (DeskerClient method only)
    """
    def inner(client: 'DeskerClient',
              img: Union[str, ImageType],
              *args, **kwargs):
        if isinstance(img, ImageType):
            img = Image.open(img)
            output = io.BytesIO()
            img.save(output)
            output.seek(0)
        elif isinstance(img, str):
            output = open(img, "rb")
        elif isinstance(img, (io._io.BytesIO, io._io.BufferedReader)):
            output = img
        else:
            raise TypeError(f"The image {img} is not valid "
                            f"(type: {type(img)})")
        return func(client, output, *args, **kwargs)

    return inner


class DeskerClient:
    """
    Attributes
    ----------
    endpoint : str
        desker server endpoint 'http://host:port'
    last_status_code : int
        HTTP status code of the last request
    last_response : requests.Response
        HTTP response of the last request
    """

    def __init__(self, endpoint: str):
        """
        Desker client (http or local library)

        Parameters
        ----------
        endpoint : str
            IP:port or hostname:port of the desker server
            (without http)

        Exceptions
        ----------
        Raise ConnectionError if the endpoint is not reachable
        """

        self.endpoint = f"http://{endpoint}"
        self.last_status_code = 200
        self.last_response = requests.Response()
        self.__check_endpoint()

    def __check_endpoint(self) -> None:
        get = None
        conn_err = requests.exceptions.ConnectionError()
        for i in range(5):
            try:
                get = requests.get(f"{self.endpoint}/")
                break
            except requests.exceptions.ConnectionError as err:
                conn_err = err
                time.sleep(1)

        if get is None:
            raise conn_err

        if get.status_code != HTTPStatus.OK:
            raise ConnectionError(
                f"Endpoint {self.endpoint} is unreachable "
                f"(response: [{get.status_code}] "
                f"{get.contents.decode()}")

    def __post(self, path: str, **kwargs) -> requests.Response:
        req = requests.post(f"{self.endpoint}{path}", **kwargs)
        self.last_status_code = req.status_code
        self.last_response = req
        return req

    @enforce_bytes
    def os_detection_from_login(
            self,
            img: Union[str, ImageType],
            proba: bool = False) -> Union[str, Dict[str, float]]:
        """
        Detect the OS from the login screenshot
        """

        req = self.__post("/os_detection/from_login",
                          files={"file": img},
                          params={"proba": proba})

        results = req.json()
        print("\nRESULTS:", results, flush=True)
        # send only the best match
        if not proba:
            return results["os"]
        # send all the probas
        return results

    @enforce_bytes
    def os_detection_is_login(
            self,
            img: Union[str, ImageType],
            proba: bool = False) -> Union[bool, float]:
        """
        Detect if screenshot corresponds to a login page
        """
        req = self.__post("/os_detection/is_login",
                          files={"file": img},
                          params={"proba": proba})

        results = req.json()
        # send proba
        if proba:
            return results["proba"]
        # send direct result
        return results["is_login"]

    @enforce_bytes
    def object_detection_all(
            self,
            img: Union[str, ImageType],
            filtered: bool = False,
            threshold: float = 0.0) -> Dict[str, list]:
        """
        Detect desktop elements (folders, buttons, apps etc.)

        Parameters
        ----------
        img : str, PIL.Image.Image
            input screenshot
        filter : bool
            Remove overlapping boxes (keep the best) and apply
            the given threshold to prune worst boxes.
        threshold : float
            Score threshold (between 0. and 1.)

        Returns
        -------

        It returns a dict with 3 keys: "boxes", "labels" and "scores".
        Each key corresponds to the caracteristic of a detected element.

        {
            boxes : [
                [xmin, ymin, xmax, ymax], ...
            ],
            labels : [
                "app1", ...
            ]
            scores : [
                0.5541, ...
            ]
        }
        """
        req = self.__post("/object_detection/all",
                          files={"file": img},
                          data={
                              "filter": filtered,
                              "threshold": threshold
                          })

        return req.json()

    @enforce_bytes
    def object_detection_single(
            self,
            img: Union[str, ImageType],
            label: str) -> Dict[str, list]:
        """
        Predicts only a single kind of object on a screenshot

        Parameters
        ----------
        image : PIL.Image.Image
            Screenshot on which prediction will be performed
        label: str
            object to predict

        Returns
        -------

        It returns a dict with 2 keys: "boxes" and "scores".
        Each key corresponds to the caracteristic of a detected element.

        {
            boxes : [
                [xmin, ymin, xmax, ymax], ...
            ],
            scores : [
                0.5541, ...
            ]
        }
        """
        req = self.__post("/object_detection/single",
                          files={"file": img},
                          data={"label": label})
        return req.json()

    @enforce_bytes
    def object_detection_all_demo(
            self,
            img: Union[str, ImageType],
            threshold: float = 0.0) -> ImageType:
        """
        Detect desktop elements (folders, buttons, apps etc.) and
        returns an annotated image. It is mainly for demo/debug
        purposes.

        Images are basically filtered (removing overlapping boxes) and
        you can tune a threshold to keep only the best matches.

        Parameters
        ----------
        img : str, PIL.Image.Image
            input screenshot
        threshold : float
            Score threshold (between 0. and 1.)

        Returns
        -------
        It returns an annotated picture
        """
        req = self.__post("/object_detection/all/demo",
                          files={"file": img},
                          params={"threshold": threshold},
                          stream=True)

        buffer = io.BytesIO(req.content)
        return Image.open(buffer)

    @enforce_bytes
    def text_detection_all(
            self,
            img: Union[str, ImageType]) -> Dict[str, list]:
        """
        Get all the text inside the image

        Parameters
        ----------
        img : str, PIL.Image.Image
            input screenshot

        Returns
        -------
        It returns a dict those keys are words and
        values are boxes where the word is found
        """
        req = self.__post("/text_detection/all",
                          files={"file": img})
        return req.json()

    @enforce_bytes
    def text_detection_links(
            self,
            img: Union[str, ImageType],
            result: str = "centers",
            link_colors: List[str] = ["#0645AD"]) -> Dict[str, list]:
        """
        """
        req = self.__post("/text_detection/links",
                          files={"file": img},
                          data={
                              "result": result,
                              "link_colors": ",".join(link_colors)}
                          )

        return req.json()

    @enforce_bytes
    def text_detection_under_box(
            self,
            img: Union[str, ImageType],
            boxes: List[Tuple[int, int, int, int]]) -> Dict[str, list]:
        """
        Detects text under boxes

        Parameters
        ----------
        img : PIL.Image.Image
            Input image
        boxes: List[Box]
            List of boxes to inspect.
            [xmin, ymin, xmax, ymax]
        """
        req = self.__post("/text_detection/under_box",
                          files={"file": img},
                          data={"boxes": json.dumps(boxes)})

        return req.json()

    @enforce_bytes
    def text_detection_blocks(
            self,
            img: Union[str, ImageType],
            min_occupation: float = 0.05,
            max_occupation: float = 0.95,
            min_aspect: float = 1 / 2,
            max_aspect: float = 2) -> List[np.ndarray]:
        """
        Find "relevant blocks" within an image, like paragraphs,
        side bars, banners etc.

        Parameters
        ----------
        img : PIL.Image.Image
            input image
        min_occupation : float
            Minimum surface of the windows in comparison to the whole image
        max_occupation : float
            Maximum surface of the windows in comparison to the whole image
        min_aspect : float
            Minimum ratio width/height
            It means that w/h > min_aspect
        max_aspect : float
            Maximum ratio width/height
            It means that w/h < max_aspect

        Returns
        -------
        masks : List[numpy.ndarray]
            List of mask corresponding to the areas
            of the detected blocks. The masks have
            the same size as the initial image
        """
        req = self.__post("/text_detection/blocks",
                          files={"file": img},
                          data={
                              "min_occupation": min_occupation,
                              "max_occupation": max_occupation,
                              "min_aspect": min_aspect,
                              "max_aspect": max_aspect})
        return req.json()

    def pipotron_generate_mail(self, mail_to: str) -> str:
        """

        """
        pass
