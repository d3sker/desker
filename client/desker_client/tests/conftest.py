
import importlib
import os
import sys
from multiprocessing import Process, set_start_method

import pytest

__WD__ = os.path.abspath(os.path.dirname(__file__))

pytest.ROOT_DIR = os.path.normpath(os.path.join(__WD__, "../../../"))
pytest.TEST_DIR = os.path.join(pytest.ROOT_DIR, "tests")
pytest.RESOURCES_DIR = os.path.join(pytest.TEST_DIR, "resources/")
pytest.OK = "[\033[32;1mOK\033[0m]"
pytest.WARNING = "[\033[33;1mWARNING\033[0m]"

sys.path.append(pytest.ROOT_DIR)
sys.path.append(os.path.join(pytest.ROOT_DIR, "client"))

desker_module = importlib.import_module("desker")
desker_app_module = importlib.import_module("desker.app")
desker_client = importlib.import_module("desker_client")


# Run multiprocessing.Process with the
# 'spawn' method
set_start_method("spawn")

# desker server variables
__HOST__ = "127.0.0.1"
__PORT__ = 50000
pytest.ENDPOINT = f"{__HOST__}:{__PORT__}"
pytest.SERVER = Process(target=desker_app_module.start,
                        args=(["--host", __HOST__, "--port", f"{__PORT__}"],),
                        daemon=True)


@pytest.fixture(scope="session")
def client():
    yield desker_client.DeskerClient(pytest.ENDPOINT)


@pytest.fixture(scope="function")
def client_banner():
    print("""\033[96m
 ██████╗██╗     ██╗███████╗███╗   ██╗████████╗
██╔════╝██║     ██║██╔════╝████╗  ██║╚══██╔══╝
██║     ██║     ██║█████╗  ██╔██╗ ██║   ██║
██║     ██║     ██║██╔══╝  ██║╚██╗██║   ██║
╚██████╗███████╗██║███████╗██║ ╚████║   ██║
 ╚═════╝╚══════╝╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝
    \033[0m""")
