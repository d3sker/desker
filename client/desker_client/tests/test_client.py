import os
import time
from http import HTTPStatus

import pytest
from desker_client import DeskerClient
from PIL.PngImagePlugin import PngImageFile

login = os.path.join(pytest.RESOURCES_DIR,
                     "login/0000_screenshot.png")
screenshot = os.path.join(pytest.RESOURCES_DIR,
                          "icon_images/test_img/0009_screenshot.png")
link = os.path.join(pytest.RESOURCES_DIR,
                    "links/2_screenshot.png")


# fancyness
try:
    __WIDTH__ = int(os.popen('stty size', 'r').read().split()[1])
except IndexError:
    __WIDTH__ = 80


def test_start_server(client_banner):
    title = "Running desker server into background ".ljust(__WIDTH__, "=")
    end = "=" * __WIDTH__
    print(f"\n\033[96m{title}\033[0m")
    pytest.SERVER.start()
    time.sleep(6)
    print(f"\033[96m{end}\033[0m")
    assert pytest.SERVER.is_alive()


def test_client_ping():
    # only create the client
    # it checks the connection is ok
    DeskerClient(pytest.ENDPOINT)


def test_os_detection(client):
    os_detected = client.os_detection_from_login(login, proba=False)
    assert client.last_status_code == HTTPStatus.OK
    assert os_detected == 'ubuntu-kde'

    client.os_detection_from_login(login, proba=True)
    assert client.last_status_code == HTTPStatus.OK

    # -------------------------------------------------- #
    is_login = client.os_detection_is_login(login)
    assert client.last_status_code == HTTPStatus.OK
    assert is_login


def test_object_detection(client):
    client.object_detection_all(screenshot)
    assert client.last_status_code == HTTPStatus.OK
    client.object_detection_all(screenshot, filtered=True)
    assert client.last_status_code == HTTPStatus.OK

    threshold = 0.25
    predictions = client.object_detection_all(screenshot,
                                              filtered=True,
                                              threshold=threshold)
    assert client.last_status_code == HTTPStatus.OK
    for s in predictions["scores"]:
        assert s >= threshold

    # -------------------------------------------------- #
    img = client.object_detection_all_demo(screenshot,
                                           threshold=threshold)
    assert client.last_status_code == HTTPStatus.OK
    assert isinstance(img, PngImageFile)

    # -------------------------------------------------- #
    predictions = client.object_detection_single(screenshot, label="folder")
    assert client.last_status_code == HTTPStatus.OK


def test_text_detection(client):
    client.text_detection_all(link)
    assert client.last_status_code == HTTPStatus.OK

    client.text_detection_links(link)
    assert client.last_status_code == HTTPStatus.OK

    client.text_detection_links(link,
                                link_colors=["#0645AD", "#0645AD"])
    assert client.last_status_code == HTTPStatus.OK

    client.text_detection_blocks(link)
    assert client.last_status_code == HTTPStatus.OK


def test_stop_server():
    title = "Terminating server ".ljust(__WIDTH__, "=")
    print(f"\n\033[96m{title}\033[0m")
    pytest.SERVER.terminate()
    time.sleep(2)
    assert not pytest.SERVER.is_alive()
