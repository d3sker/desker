from setuptools import find_packages, setup

# desker metadata ---------------------------------------------------------- #
name = "desker_client"
version = "1.0a0"  # the version must match Desker's version
release = "1.0"
# -------------------------------------------------------------------------- #


setup(
    name=name,
    version=version,
    description="Desker app client",
    packages=find_packages(),
    include_package_data=True,
    author=["asr"],
    author_email=["contact@amossys.fr"],
    url="https://gitlab.com/d3sker/desker",
    tests_require=[
        'pytest',
    ],
    install_requires=open("requirements.txt").read().splitlines(),
)
